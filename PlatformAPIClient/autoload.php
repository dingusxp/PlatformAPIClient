<?php

/**
 * 注册类库自动加载规则
 * @param type $class
 */
function platform_api_client_autoload($class) {
    static $_componentPrefix = 'PlatformAPIClient_';
    
    if (preg_match("/^$_componentPrefix/", $class)) {
        include_once dirname(__FILE__).'/'.str_replace($_componentPrefix, '', $class).'.php';
    }
}

spl_autoload_register('platform_api_client_autoload');
