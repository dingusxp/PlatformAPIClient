<?php

/**
 * 统一抽象类
 */
abstract class PlatformAPIClient_Abstract {
    
    /**
     * 参数信息
     * @var type 
     */
    protected $_option = array();
    
    /**
     * 构造函数
     * @param type $option
     */
    public function __construct($option = array()) {
        
        $this->_option = $option;
    }
    
    /**
     * 获取订单列表
     */
    abstract public function getOrderList($conditions);

    /**
     * 获取指定订单信息
     */
    abstract public function getOrder($orderId);
    
    /**
     * 设置库存
     */
    abstract public function setStock($sku, $stock);
    
}