<?php

/**
 * shopee  
 */
class PlatformAPIClient_Shopee extends PlatformAPIClient_Abstract {
    
    /**
     * client
     * @var type 
     */
    private $_client = null;
    
    /**
     * 初始化
     * @param type $option
     */
    public function __construct($option = array()) {
        
        parent::__construct($option);

        require_once dirname(dirname(__FILE__)) . '/third-lib/shopee-sdk/autoload.php';
        $this->_client = new \Shopee\Client([
            'baseUrl' => 'https://partner.shopeemobile.com',
            'secret' => $option['secret'],
            'partner_id' => $option['partner_id'],
            'shopid' => $option['shopid'],
        ]);
    }

    /**
     * 根据条件获取订单列表
     * @param type $conditions
     *  + order_status   string  ALL/UNPAID/READY_TO_SHIP/COMPLETED/IN_CANCEL/CANCELLED/TO_RETURN
     *  + create_time_from  int  timestamp
     *  + create_time_to  int  timestamp
     *  + pagination_offset   int  default: 0
     *  + pagination_entries_per_page  int 
     * @return array
     */
    public function getOrderList($conditions) {
        
        $param = array(
//            'order_status' => 'READY_TO_SHIP',
//            "create_time_from" => time() - 86400*3,
//            "create_time_to" => time(),
            "pagination_offset" => 0,
            "pagination_entries_per_page" => 20,
        );
        $param = array_merge($param, $conditions);
        $response = $this->_client->order->getOrdersByStatus($param);
        $data = $response->getData();
        if (empty($data['orders'])) {
            return [];
        }
        $ordersnList = [];
        foreach ($data['orders'] as $item) {
            $ordersnList[] = $item['ordersn'];
        }
        $param = ['ordersn_list' => $ordersnList];
        $response = $this->_client->order->getOrderDetails($param);
        $data = $response->getData();
        return $data['orders'];
    }

    /**
     * 获取指定订单信息
     * @param type $orderId
     * @return array
     */
    public function getOrder($orderId) {

        $param = array(
            'ordersn_list' => [$orderId],
        );
        $response = $this->_client->order->getOrderDetails($param);
        $data = $response->getData();
        if (empty($data['orders'])) {
            return [];
        }
        $order = $data['orders'][0];

        // with escrow info
        $order['escrow_info'] = $this->getOrderEscrow($orderId);

        return $order;
    }

    /**
     * 获取指定订单对账费用信息
     * @param type $orderId
     * @return array
     */
    public function getOrderEscrow($orderId) {

        $param = array(
            "ordersn" => $orderId,
        );
        $response = $this->_client->order->getEscrowDetails($param);
        $data = $response->getData();
        if (empty($data['order'])) {
            return [];
        }
        $order = $data['order'];

        return $order;
    }

    /**
     * 设置指定 sku 在平台的库存量
     * @param type $sku
     * @param type $stock
     */
    public function setStock($sku, $stock) {
        
        if (!isset($sku['spu_id'])) {
            throw new Exception('invalid sku info: '.json_encode($sku));
        }
        if ($stock < 0) {
            throw new Exception('invalid stock: '.$stock);
        }
        
        if (!empty($sku['sku_id'])) {
            $param = array(
                'item_id' => intval($sku['spu_id']),
                'variation_id' => intval($sku['sku_id']),
                'stock' => $stock,
            );
//            echo 'shopee update variation stock: ', print_r($param, true), PHP_EOL; return;
            $response = $this->_client->item->updateVariationStock($param);
            $data = $response->getData();
            return $data;
        } else {
            $param = array(
                'item_id' => intval($sku['spu_id']),
                'stock' => $stock,
            );
//            echo 'shopee update stock: ', print_r($param, true), PHP_EOL; return;
            $response = $this->_client->item->updateStock($param);
            $data = $response->getData();
//            print_r($data);
            return $data;
        }
    }
}