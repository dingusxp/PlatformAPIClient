<?php

/**
 * 电商平台 相关配置
 */
return array(
    
    // ===== shopee ===== //
    'shopee-aaaaaa' => [
        'platform' => 'shopee',
        'option' => [
            'secret' => 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
            'partner_id' => 111111,
            'shopid' => 222222,
        ],
    ],
    
    // ===== lazada ===== //
    'lazada-bbbbbb' => [
        'platform' => 'lazada',
        'option' => [
            'app_key' => '111111',
            'app_secret' => 'xxxxxx',
            'shop_name' => 'lazada-bbbbb',
            'api_url' => 'https://api.lazada.co.id/rest', // 各个国家不一样，按需配置
            'code_callback' => 'get_my_lazada_auth_code', // 按需实现回调函数，获取店铺授权后的 code
        ],
    ],
    
    // ===== tokopedia ===== //
    'tokopedia-cccccc' => [
        'platform' => 'tokopedia',
        'option' => [
            'app_key' => 'xxxxxxxx',
            'app_secret' => 'ssssssss',
            'fs_id' => '111111',
            'shop_id' => '222222',
//            'debug_mode' => true,
        ],
    ],
    
    // ===== jdid ===== //
    'jdid-dddddd' => [
        'platform' => 'jdid',
        'option' => [
            'app_key' => 'xxxxxxxx',
            'app_secret' => 'ssssssss',
            'token' => 'tttttt',
//            'debug_mode' => true,
        ],
    ],
    
    // ====== blibli ====== //
    'blibli-eeeeee' => [
        'platform' => 'blibli',
        'option' => [
            'api_username' => 'xxxxxx',//your API username
            'api_passwd' => 'xxxxxx',//your API password
            'api_secret' => 'sssssss',//your API secret key
            'mta_username' => 'eeeeee',//your MTA username
            'mta_passwd' => 'xxxxxx',//your MTA password
            'platform_name' => 'AppName',//your company name/platform name
            'partner_code' => 'xxxxx',//your Business Partner Code / Merchant Code
            'timeout_time' => 5,//your request timeout
//            'debug_mode' => true,
        ],
    ],
    
);
