<?php
/**
 * demo
 */

// 引入：可以直接包含 或者 使用 composer
require_once dirname(dirname(__FILE__)).'/PlatformAPIClient.php';

// 方法一：直接构造client并调用
/*
try {
    $platform = 'shopee';
    $option = [
        'secret' => 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
        'partner_id' => 111111,
        'shopid' => 222222,
    ];
    $conditions = [
        'order_status' => 'READY_TO_SHIP',
        'create_time_from' => time() - 86400*3,
        'create_time_to' => time(),
        'pagination_entries_per_page' => 5,
    ];
    $client = PlatformAPIClient::factory($platform, $option);
    $orders = $client->getOrderList($conditions);
    file_put_contents(dirname(__FILE__).'/orders.txt', json_encode($orders, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
    echo 'done', PHP_EOL;
} catch (Exception $ex) {
    echo 'Exception: ', $ex->getMessage(), PHP_EOL;
}
exit;// ====================== //
 */

// 方法二：（建议）统一配置，并通过 shop name 去获取调用client
$shopConfigs = include(dirname(__FILE__)).'/shop_configs.php';
PlatformAPIClient::setShopConfigs($shopConfigs);
try {
    /*
     */
    // shopee
    $shopName = 'shopee-aaaaaa';
    $conditions = [
        'order_status' => 'READY_TO_SHIP',
        'create_time_from' => time() - 86400*3,
        'create_time_to' => time(),
        'pagination_entries_per_page' => 5,
    ];
    /*
    // 说明：lazada 调用太麻烦，此处未测试，请自行验证
    // lazada
    $shopName = 'lazada-bbbbbb';
    $conditions = [
        'status' => 'pending',
        'created_after' => date('Y-m-d', time() - 86400 * 3) . 'T00:00:00+07:00',
        'offset' => 0,
        'limit' => 5,
    ];
     */
    /* 
    // tokopedia
    $shopName = 'tokopedia-cccccc';
    $conditions = [
        'from_date' => time() - 2 * 86400,
        'to_date' => time() + 86400,
        'per_page' => 5,
        'status' => 400,
    ];
     */
    /*
    // jd.id
    $shopName = 'jdid-dddddd';
    $conditions = [
		"orderStatus" => 1,
		"createdTimeBegin" => time() - 86400 * 3,
		"startRow" => 0, // 注意：jd.id 每页数目固定为 20
    ];
     */
    /*
    // blibli
    $shopName = 'blibli-eeeeee';
    $conditions = [
        "status" => "FP",
        "filterStartDate" => date("Y-m-d H:i:s", time() - 86400*3),
        'filterEndDate' => date('Y-m-d H:i:s'),
        'page' => 0,
        'size' => 5,
    ];
     */
    $client = PlatformAPIClient::getClientByShop($shopName);
    $orders = $client->getOrderList($conditions);
    file_put_contents(dirname(__FILE__).'/orders.txt', json_encode($orders, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
    echo 'done', PHP_EOL;
} catch (Exception $ex) {
    echo 'Exception: ', $ex->getMessage(), PHP_EOL;
}