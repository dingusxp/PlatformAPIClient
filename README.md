# PlatformAPIClient

#### 介绍
> 东南亚（特别针对印尼）主要电商平台（shopee, lazada, tokopedia, blibli, jd.id, akulaku）的开放接口调用。

#### 代码说明
- PlatformAPIClient.php   接口统一调用入口
- PlatformAPIClient/*     按平台的统一接口实现
- third-lib               各平台原始的 Client，其中部分是官方 sdk，部分是根据官方协议用 php curl 实现
- demo                    示例样例

注意：PlatformAPIClient 里目前只实现了少量几个 API，如 订单拉取，库存设置；
但是基础架子都有，可以按需根据官方文档快速扩展其他需要的接口。

#### 使用说明
##### 直接引用：
下载类库，放置在任意目录，如： app-path/lib/
```php
// 详细请参见 demo/demo.php

require_once dirname(dirname(__FILE__)).'/PlatformAPIClient.php';
try {
    $platform = 'shopee';
    $option = [
        'secret' => 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
        'partner_id' => 111111,
        'shopid' => 222222,
    ];
    $client = PlatformAPIClient::factory($platform, $option);
    $conditions = [
        'pagination_entries_per_page' => 5,
    ];
    $orders = $client->getOrderList($conditions);
    file_put_contents(dirname(__FILE__).'/orders.txt', json_encode($orders, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
    echo 'done', PHP_EOL;
} catch (Exception $ex) {
    echo 'Exception: ', $ex->getMessage(), PHP_EOL;
}

```

##### 使用 composer

直接 require

```json

    "require": {
        "dingusxp/platform_api_client": "dev-master"
    }

```

或者下载代码到本地，手动配置 autoload 引用
```json

    "autoload": {
        "psr-4": { "": ["path-to/PlatformAPIClient"] }
    },

```

#### 各平台申请 API 简要指引

##### shopee
API 文档： https://open.shopee.com/  

开发授权：按引导注册完成，登录即可获得调用密钥。

店铺授权：

参照官方文档 https://open.shopee.com/documents?module=63&type=2&id=51 （Step 5: Shop Authorization）生成一个授权链接，发给店铺管理员，他登录确认后即完成授权。

其他：

官方还提供了两个好工具，是全部平台里，对开发者最友好的。
1. 沙盒环境测试；
2. 消息推送模式对接。

##### lazada
API 文档：https://open.lazada.com/

开发授权：按引导要求注册登录。填写 profile 信息并审核通过后，可以创建 APP。

店铺授权：
1. 在 APP 里配置店铺授权后的回调接收URL；
2. 在 授权管理 里添加 店铺，需要 SellerID 和 账号登录信息；
3. 按文档（Developer's Guide > Seller authorization introduction）生成授权链接发送给店铺方；
4. 店铺方登录允许授权后，会自动跳转到 1 所设置的URL，参数里带一个 code；将该 code 记录到系统，进行 API 授权，即可开始调用 API 获取该店铺相关数据；
5. 授权有有效期，因此需要定期进行操作 4 授权。。。

其他：
- lazada 要求必须要配置调用服务器的 IP白名单；
- lazada 接口调用通过，上线完成，lazada 还有一个考察期；审核通过前，接口调用次数和授权有效期都有限制；
- lazada 获取订单信息时，得到的用户信息时 打码 的，要想获取到完整信息，需要申请 data moat，又是一堆流程和审核。

##### tokopedia
API 文档：https://developer.tokopedia.com/openapi/guide/#/

开发和店铺授权：
一般是商务线下沟通（需要花钱购买服务），然后他们会发邮件，告知 fs_id 和 app_key 等信息。

##### blibli
文档：https://merchant-api.blibli.com/docs/sections/60/contents/2867?title=introduction

开发和店铺授权： 使用店铺账号登录即可，然后创建 APP

##### JD.ID
文档：http://api.jd.id/ctrlCenter/appList

开发和店铺授权：使用店铺账号登录即可，然后创建 APP

