<?php

namespace Shopee\Nodes\ShopCategory;

use Shopee\Nodes\NodeAbstract;
use Shopee\RequestParametersInterface;
use Shopee\ResponseData;

class ShopCategory extends NodeAbstract
{
    /**
     * Use this call to get categories of shop.
     *
     * @param array|RequestParametersInterface $parameters
     * @return ResponseData
     */
    public function getShopCategories($parameters = []): ResponseData
    {
        return $this->post('/api/v1/shop_categorys/get', $parameters);
    }

}
